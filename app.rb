require 'pry-byebug'
require 'sinatra'
require 'sinatra/json'
require 'sinatra/cross_origin'
require 'dropbox_sdk'

require_relative './image_parser'
require_relative './image_downloader'

#### CORS

configure do
  enable :cross_origin
end

options "*" do
  response.headers["Allow"] = "HEAD,GET,PUT,POST,DELETE,OPTIONS"
  response.headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Cache-Control, Accept"

  200
end

####

DROPBOX_ACCESS_TOKEN = "DMaZyMJziMUAAAAAAABT0Z6zZ0goqNB4Neq6XaO_OiRXbDeGnkybCMyLuF8ftdR4"

get "/" do
  erb :index, :locals => {
    :foo => "bar"
  }
end

get "/gps" do
  response = []

  Dir['public/images/*'].each do |image|
    position = ImageParser.new(image).parse_image
    response.push(position) if position
  end

  json(response)
end

get "/sync" do
  client = DropboxClient.new(DROPBOX_ACCESS_TOKEN)
  metadata = client.metadata('/images')["contents"]
  local, downloaded = ImageDownloader.new(client, metadata).sync
  { :local => local, :downloaded => downloaded }.to_json
end
