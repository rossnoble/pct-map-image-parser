require 'pry-byebug'
require 'exifr'

class ImageParser
  attr_reader :image

  def initialize(image)
    @image = image
  end

  def parse_image
    metadata = EXIFR::JPEG.new(image)
    gps = metadata.gps
    return unless gps
    
    {
      :path => image.split('/').slice(1, 2).join('/'),
      :date => metadata.date_time,
      :lat => gps.latitude,
      :lng => gps.longitude
    }
  end
end
