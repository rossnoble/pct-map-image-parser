class ImageDownloader
  attr_reader :client, :metadata

  def initialize(client, metadata)
    @client   = client
    @metadata = metadata
  end

  def sync
    local_files = []
    files_downloaded = []

    metadata.each do |image|
      image_path = image['path']
      local_path = "public" + image_path

      if File.exists?(local_path)
        local_files << image_path
      else
        contents, image_meta = client.get_file_and_metadata(image_path)
        open(local_path, 'w') { |f| f.puts contents }
        files_downloaded << image_path
      end
    end

    [local_files, files_downloaded]
  end
end
